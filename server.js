var express = require('express'),
  app = express(),
  port = process.env.PORT || 3001;

var bodyParser = require('body-parser');
app.use(bodyParser.json())
app.use(function(req, res, next) {
 res.header("Access-Control-Allow-Origin", "*");
 res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
 next();
});
var requestjson = require('request-json');

var path = require('path');
var urlmovimientosMlab = "https://api.mlab.com/api/1/databases/bdbanca2rrh/collections/movimientos?apiKey=k_z1o97qvqYdpYkwKd_VwkClOJXr9OH-";
var clienteMLab = requestjson.createClient(urlmovimientosMlab);
var postbody = {"idcliente": 1111,"nombre": "Prueba","apellido": "Pruebosa"}

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get("/",function(req,res){
  res.sendFile(path.join(__dirname,'index.html'));
})

app.get("/movimientos",function(req,res){
  clienteMLab.get('',function(err, resM, body){
    if(err){
      console.log(body)
    }else{
      res.send(body)
    }
  })
})

//Se agrego petición POST para alta en BD de Mongo 21 Ago
//Se agrego parametro req.body para mandar parámetros 22 Ago
app.post("/movimientos",function(req,res){
  clienteMLab.post('',req.body,function(err, resM, body){
    if(err){
      console.log(body)
    }else{

      res.send(body)
    }
  })
})

app.get("/clientes/:idcliente",function(req,res){
  res.send("Aquí tiene el cliente número: " + req.params.idcliente);
})

app.post("/",function(req,res){
  res.send("Hemos recibido su petición POST actualizada")

})

app.put("/",function(req,res){
  res.send("Hemos recibido su petición PUT")
})

app.patch("/",function(req,res){
  res.send("Hemos recibido su petición PATCH")
})

app.delete("/",function(req,res){
  res.send("Hemos recibido su petición DELETE")
})
